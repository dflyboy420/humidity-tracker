import os from 'os';
import fs from 'fs';
import Redis from 'ioredis';

let client = new Redis(process.env.REDIS);

export default class Instance {
    id: string;
    room: string = process.env.ROOM;
    type: string;
    hostname: string;

    constructor() {
        this.id = fs.readFileSync('/etc/machine-id', 'utf8').trim();
        this.hostname = os.hostname();
        this.type = 'sensor';
    }

    get uptime() {
        return os.uptime();
    }

    get load() {
        return os.loadavg()[0];
    }

    get redis() {
        return client;
    }

    async getTemp() {
        const out =await fs.promises.readFile('/sys/class/thermal/thermal_zone0/temp', 'ascii');
        return (<number><unknown>out)/1000;
    }
    
}