import EventEmitter from 'events';
import dht from 'node-dht-sensor';
import logger from './logger';
import gpio from 'rpi-gpio';

export function measureAir(): Promise<{ temperature: number, humidity: number }> {
    return new Promise((resolve, reject) => {

        const start = performance.now();

        dht.read(11, parseInt(process.env.DHT_PIN), (err, temperature, humidity) => {
            const duration = performance.now() - start;

            logger.info('vals measured', { temperature, humidity, duration })

            return resolve({ temperature, humidity });
        });
    });

}

export class MotionSensor extends EventEmitter {
    pin: number;

    constructor() {
        super();
        this.pin = parseInt(process.env.HC_PIN);

        gpio.setMode('mode_bcm')
        gpio.on('change', this.onChange);
        gpio.setup(this.pin, gpio.DIR_IN, gpio.EDGE_BOTH);

    }

    async read() {
        return await gpio.promise.read(this.pin);
    }

    onChange(channel: number, value: number) {
        if(channel === this.pin) {
            this.emit('change', value);
        }
    }
}
