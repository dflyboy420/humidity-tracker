import 'dotenv/config';

import { floor, pick } from 'lodash';
import config from 'config';
import Instance from './instance';
import logger from './logger';
import { measureAir, MotionSensor } from './sensors';
import Display from './display';
import { MongoClient, Db, Collection } from 'mongodb';

const motionSensor = new MotionSensor();
const instance = new Instance();
const display = new Display();
const client = new MongoClient(process.env.MONGODB, {
    ssl: true
});
let db: Db;

class Transmitter {

    async submitData() {
        const payload = {
            instance,
            data: {
                device_temp: await instance.getTemp(), load: instance.load,
                ... await measureAir(),
                motion: Number(await motionSensor.read()),
            }
        };

        const time = floor(Date.now() / 100);
        display.devTemp = payload.data.device_temp;
        display.load = payload.data.load;
        display.humidity = payload.data.humidity;
        display.temperature = payload.data.temperature;
        display.updateVals();


        const coll = db.collection('sensors');
        await coll.insertOne({
            time: new Date(),
            instance: pick(instance, ['id', 'room']),
            ...payload.data
        });

        await instance.redis.hmset('msrmt:'+instance.room, {
            instance: instance.id,
            ...payload.data,
            time
        });
    }
}


(async function main() {
    logger.info('started instance', instance);
    await client.connect();
    // Establish and verify connection
    await client.db("home").command({ ping: 1 });
    logger.info("mongo connected");
    db = client.db("home");

    motionSensor.on('change', val => {
        display.motionState = Number(val);
        display.updateVals();
    });
    setInterval(async () => {
        display.motionState = Number(await motionSensor.read());
        display.updateVals();
    }, 1000);


    const transmitter = new Transmitter();

    setInterval(transmitter.submitData, 2000);

})();
