import i2c from 'i2c-bus';
const i2cBus = i2c.openSync(1);

import OLED from 'oled-i2c-bus';
import FontPack from 'oled-font-pack';

let font5x7 = FontPack.oled_5x7;
let font8x12 = FontPack.small_8x12;

function padTo2Digits(num: number) {
    return num.toString().padStart(2, '0');
}

function formatTime(date: Date) {
    return (
      [
        padTo2Digits(date.getHours()),
        padTo2Digits(date.getMinutes()),
        padTo2Digits(date.getSeconds()),
      ].join(':')
    );
  }

export default class Display {
    oled: any;
    private _temp: number;
    private _load: number;
    private _humidity: number;
    private _devTemp: number;
    private _motionState: number;
    private _motionStateTime: Date;
    
    public get motionState(): number {
        return this._motionState;
    }
    public set motionState(value: number) {
        this._motionStateTime = new Date()
        this._motionState = value;
    }

    public get humidity(): number {
        return this._humidity;
    }
    public set humidity(value: number) {
        this._humidity = value;
    }

    public get devTemp(): number {
        return this._devTemp;
    }
    public set devTemp(value: number) {
        this._devTemp = value;
    }


    public get load(): number {
        return this._load;
    }
    public set load(value: number) {
        this._load = value;
    }

    public get temperature(): number {
        return this._temp;
    }
    public set temperature(value: number) {
        this._temp = value;
    }

    constructor(address = 0x3c) {
        const opts = {
            width: 128,
            height: 64,
            address
        };

        this.oled = new OLED(i2cBus, opts);

        this.oled.turnOnDisplay();
        this.oled.clearDisplay();

        setInterval(() => {this.showTime() }, 1000);
    }

    showTime() {
        this.oled.setCursor(28, 1);
        this.oled.writeString(font8x12, 1, formatTime(new Date()), 1);
        this.oled.drawLine(1, 14, 128, 14, 1);
        this.oled.update();
    }

    updateVals() {
        this.oled.setCursor(1, 16);
        let str = `Air: ${this.temperature}C  ${this.humidity}%\n`;
        str += `CPU: ${this.devTemp.toFixed(1)}C  ${this.load}\n`;
        str += `Motion: ${this.motionState} (${formatTime(this._motionStateTime)})`;
        

        this.oled.writeString(font5x7, 1, str, 1, true);
        this.oled.update();
    }

}