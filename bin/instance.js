"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const os_1 = __importDefault(require("os"));
const fs_1 = __importDefault(require("fs"));
const ioredis_1 = __importDefault(require("ioredis"));
let client = new ioredis_1.default(process.env.REDIS);
class Instance {
    constructor() {
        this.room = process.env.ROOM;
        this.id = fs_1.default.readFileSync('/etc/machine-id', 'utf8').trim();
        this.hostname = os_1.default.hostname();
        this.type = 'sensor';
    }
    get uptime() {
        return os_1.default.uptime();
    }
    get load() {
        return os_1.default.loadavg()[0];
    }
    get redis() {
        return client;
    }
    async getTemp() {
        const out = await fs_1.default.promises.readFile('/sys/class/thermal/thermal_zone0/temp', 'ascii');
        return out / 1000;
    }
}
exports.default = Instance;
//# sourceMappingURL=instance.js.map