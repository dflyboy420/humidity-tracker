"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const i2c_bus_1 = __importDefault(require("i2c-bus"));
const i2cBus = i2c_bus_1.default.openSync(1);
const oled_i2c_bus_1 = __importDefault(require("oled-i2c-bus"));
const oled_font_pack_1 = __importDefault(require("oled-font-pack"));
let font5x7 = oled_font_pack_1.default.oled_5x7;
let font8x12 = oled_font_pack_1.default.small_8x12;
function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}
function formatTime(date) {
    return ([
        padTo2Digits(date.getHours()),
        padTo2Digits(date.getMinutes()),
        padTo2Digits(date.getSeconds()),
    ].join(':'));
}
class Display {
    get motionState() {
        return this._motionState;
    }
    set motionState(value) {
        this._motionStateTime = new Date();
        this._motionState = value;
    }
    get humidity() {
        return this._humidity;
    }
    set humidity(value) {
        this._humidity = value;
    }
    get devTemp() {
        return this._devTemp;
    }
    set devTemp(value) {
        this._devTemp = value;
    }
    get load() {
        return this._load;
    }
    set load(value) {
        this._load = value;
    }
    get temperature() {
        return this._temp;
    }
    set temperature(value) {
        this._temp = value;
    }
    constructor(address = 0x3c) {
        const opts = {
            width: 128,
            height: 64,
            address
        };
        this.oled = new oled_i2c_bus_1.default(i2cBus, opts);
        this.oled.turnOnDisplay();
        this.oled.clearDisplay();
        setInterval(() => { this.showTime(); }, 1000);
    }
    showTime() {
        this.oled.setCursor(28, 1);
        this.oled.writeString(font8x12, 1, formatTime(new Date()), 1);
        this.oled.drawLine(1, 14, 128, 14, 1);
        this.oled.update();
    }
    updateVals() {
        this.oled.setCursor(1, 16);
        let str = `Air: ${this.temperature}C  ${this.humidity}%\n`;
        str += `CPU: ${this.devTemp.toFixed(1)}C  ${this.load}\n`;
        str += `Motion: ${this.motionState} (${formatTime(this._motionStateTime)})`;
        this.oled.writeString(font5x7, 1, str, 1, true);
        this.oled.update();
    }
}
exports.default = Display;
//# sourceMappingURL=display.js.map