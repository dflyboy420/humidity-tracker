"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const lodash_1 = require("lodash");
const instance_1 = __importDefault(require("./instance"));
const logger_1 = __importDefault(require("./logger"));
const sensors_1 = require("./sensors");
const display_1 = __importDefault(require("./display"));
const mongodb_1 = require("mongodb");
const motionSensor = new sensors_1.MotionSensor();
const instance = new instance_1.default();
const display = new display_1.default();
const client = new mongodb_1.MongoClient(process.env.MONGODB, {
    ssl: true
});
let db;
class Transmitter {
    async submitData() {
        const payload = {
            instance,
            data: {
                device_temp: await instance.getTemp(), load: instance.load,
                ...await (0, sensors_1.measureAir)(),
                motion: Number(await motionSensor.read()),
            }
        };
        const time = (0, lodash_1.floor)(Date.now() / 100);
        display.devTemp = payload.data.device_temp;
        display.load = payload.data.load;
        display.humidity = payload.data.humidity;
        display.temperature = payload.data.temperature;
        display.updateVals();
        const coll = db.collection('sensors');
        await coll.insertOne({
            time: new Date(),
            instance: (0, lodash_1.pick)(instance, ['id', 'room']),
            ...payload.data
        });
        await instance.redis.hmset('msrmt:' + instance.room, {
            instance: instance.id,
            ...payload.data,
            time
        });
    }
}
(async function main() {
    logger_1.default.info('started instance', instance);
    await client.connect();
    // Establish and verify connection
    await client.db("home").command({ ping: 1 });
    logger_1.default.info("mongo connected");
    db = client.db("home");
    motionSensor.on('change', val => {
        display.motionState = Number(val);
        display.updateVals();
    });
    setInterval(async () => {
        display.motionState = Number(await motionSensor.read());
        display.updateVals();
    }, 1000);
    const transmitter = new Transmitter();
    setInterval(transmitter.submitData, 2000);
})();
//# sourceMappingURL=index.js.map