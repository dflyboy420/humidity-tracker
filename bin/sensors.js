"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MotionSensor = exports.measureAir = void 0;
const events_1 = __importDefault(require("events"));
const node_dht_sensor_1 = __importDefault(require("node-dht-sensor"));
const logger_1 = __importDefault(require("./logger"));
const rpi_gpio_1 = __importDefault(require("rpi-gpio"));
function measureAir() {
    return new Promise((resolve, reject) => {
        const start = performance.now();
        node_dht_sensor_1.default.read(11, parseInt(process.env.DHT_PIN), (err, temperature, humidity) => {
            const duration = performance.now() - start;
            logger_1.default.info('vals measured', { temperature, humidity, duration });
            return resolve({ temperature, humidity });
        });
    });
}
exports.measureAir = measureAir;
class MotionSensor extends events_1.default {
    constructor() {
        super();
        this.pin = parseInt(process.env.HC_PIN);
        rpi_gpio_1.default.setMode('mode_bcm');
        rpi_gpio_1.default.on('change', this.onChange);
        rpi_gpio_1.default.setup(this.pin, rpi_gpio_1.default.DIR_IN, rpi_gpio_1.default.EDGE_BOTH);
    }
    async read() {
        return await rpi_gpio_1.default.promise.read(this.pin);
    }
    onChange(channel, value) {
        if (channel === this.pin) {
            this.emit('change', value);
        }
    }
}
exports.MotionSensor = MotionSensor;
//# sourceMappingURL=sensors.js.map